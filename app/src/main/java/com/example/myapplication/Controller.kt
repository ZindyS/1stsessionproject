package com.example.myapplication

import java.util.*

//19.04.2023
//Солодков Роман
//Класс, содержащий очередь
class Controller {
    val queue : Queue<Pair<String, Int>> = LinkedList()

    //Функция получения размера
    fun getSize() : Int {
        return queue.size
    }

    //Функция получения элемента
    fun getElement() : Pair<String, Int> {
        return queue.element()
    }

    //Функция удаления элемента
    fun deleteItem() {
        queue.remove()
    }

    //Функция добавления элемента
    fun addElement(element : Pair<String, Int>) {
        queue.add(element)
    }
}