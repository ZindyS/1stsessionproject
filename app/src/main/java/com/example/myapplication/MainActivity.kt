package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.myapplication.databinding.ActivityMainBinding

//19.04.2023
//Солодков Роман
//Класс описания функционала стартового экрана
class MainActivity : AppCompatActivity() {
    lateinit var binding : ActivityMainBinding
    val controller = Controller()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val list = listOf(Pair("first", R.drawable.first), Pair("second", R.drawable.second), Pair("third", R.drawable.third))
        for (i in list) {
            controller.addElement(i)
        }

        val pair = controller.getElement()
        binding.imageView.setImageResource(pair.second)
        binding.textView.text = pair.first

        binding.button.setOnClickListener {
            controller.deleteItem()
            if (controller.getSize()==0) {
                val intent = Intent(this, SignInActivity::class.java)
                startActivity(intent)
            } else if (controller.getSize()==1) {
                binding.button.text = "Начать"
                val pair = controller.getElement()
                binding.imageView.setImageResource(pair.second)
                binding.textView.text = pair.first
            } else {
                val pair = controller.getElement()
                binding.imageView.setImageResource(pair.second)
                binding.textView.text = pair.first
            }
        }
    }
}