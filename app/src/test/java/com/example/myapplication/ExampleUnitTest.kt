package com.example.myapplication

import org.junit.Test

import org.junit.Assert.*

//19.04.2023
//Солодков Роман
//Класс с Unit-тестами
class ExampleUnitTest {
    //Проверка на то, что размер очереди изменяется нормально
    @Test
    fun isSizeCorrect() {
        val controller = Controller()
        val list = listOf(Pair("first", R.drawable.first), Pair("second", R.drawable.second), Pair("third", R.drawable.third))
        for (i in list) {
            controller.addElement(i)
        }
        val sz = controller.getSize()
        controller.deleteItem()
        assertEquals(controller.getSize(), sz-1)
    }

    //Проверка, что все элементы в очереди вытаскиваются в правильном порядке
    @Test
    fun orderCheck() {
        val controller = Controller()
        val list = listOf(Pair("first", R.drawable.first), Pair("second", R.drawable.second), Pair("third", R.drawable.third))
        for (i in list) {
            controller.addElement(i)
        }
        for (i in list) {
            assertEquals(controller.getElement(), i)
            controller.deleteItem()
        }
    }
}