package com.example.myapplication

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.junit.Rule

//19.04.2023
//Солодков Роман
//Класс с UI тестами
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {

    @JvmField
    @Rule
    val scenario = ActivityScenarioRule(MainActivity::class.java)

    //Проверка, что в начале кнопка имеет текст "Далее"
    @Test
    fun checkButtonStart() {
        val button = onView(withId(R.id.button))
        button.check(matches(withText("Далее")))
    }

    //Проверка, что кнопка меняет свой текст на "Начать", когда у неё остаётся один экран
    @Test
    fun checkButton() {
        val button = onView(withId(R.id.button))
        button.perform(click())
        button.perform(click())
        button.check(matches(withText("Начать")))
    }

    //Проверка, что при нажатии на кнопку начать идёт переход на экран SignIn
    @Test
    fun checkNewActivity() {
        val button = onView(withId(R.id.button))
        button.perform(click())
        button.perform(click())
        button.perform(click())
        val back = onView(withId(R.id.back))
        back.check(matches(isDisplayed()))
    }
}